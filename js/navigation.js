$(function() {
  $(window).on('scroll load', function() {
    var nav = $('.navbar');
    if (nav.offset().top) {
      if (!nav.hasClass('opaque')) {
        nav.addClass('opaque');

      }
    } else {
      nav.removeClass('opaque');
    }

  })

  $('.carousel').carousel({
    interval: 4000
  });

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
